﻿using Microsoft.AspNetCore.Mvc;
using TestTaskRunningCodeApi.Dtos;
using TestTaskRunningCodeApi.Services;

namespace TestTaskRunningCodeApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class UsersController: ControllerBase
{
    private readonly IUserService _service;

    public UsersController(IUserService service)
    {
        _service = service;
    }

    [HttpGet]
    public async Task<IEnumerable<UserOdto>> GetList()
    {
        var result = await _service.GetList();
        return result;
    }

    [HttpPost]
    [ProducesResponseType(201)]
    public async Task<UserOdto> Post(UserIdto idto)
    {
        var result = await _service.Add(idto);
        Response.StatusCode = 201;
        return result;
    }

    [HttpPut("{id}")]
    [ProducesResponseType(204)]
    public async Task Put(int id, UserIdto idto)
    {
        await _service.Edit(id, idto);
        Response.StatusCode = 204;
    }
}