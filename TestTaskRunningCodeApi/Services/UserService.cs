﻿using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestTaskRunningCodeApi.Dtos;
using TestTaskRunningCodeApi.Entities;
using TestTaskRunningCodeApi.Exceptions;

namespace TestTaskRunningCodeApi.Services;

public class UserService: IUserService
{
    protected readonly AppDbContext _context;

    public UserService(AppDbContext context)
    {
        _context = context;
    }

    public async Task<IEnumerable<UserOdto>> GetList()
    {
        var result = await _context.Users
            .Select(x => new UserOdto()
            {
                Id = x.Id,
                CreatedDate = x.CreatedDate,
                UpdatedDate = x.UpdatedDate,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email,
                MobileCountryCode = x.MobileCountryCode,
                MobilePhoneNumber = x.MobilePhoneNumber,
                IsActive = x.IsActive
            })
            .ToListAsync();
        return result;
    }
    
    public async Task<UserOdto> Add(UserIdto dto)
    {
        var entity = new User
        {
            FirstName = dto.FirstName,
            LastName = dto.LastName,
            Email = dto.Email,
            Password = DoHash(dto.Password),
            MobileCountryCode = dto.MobileCountryCode,
            MobilePhoneNumber = dto.MobilePhoneNumber,
            IsActive = dto.IsActive
        };
        await _context.Users.AddAsync(entity);
        await _context.SaveChangesAsync();

        var result = new UserOdto
        {
            Id = entity.Id,
            CreatedDate = entity.CreatedDate,
            UpdatedDate = entity.UpdatedDate,
            FirstName = entity.FirstName,
            LastName = entity.LastName,
            Email = entity.Email,
            MobileCountryCode = entity.MobileCountryCode,
            MobilePhoneNumber = entity.MobilePhoneNumber,
            IsActive = entity.IsActive
        };
        return result;
    }
    
    public async Task Edit(int id, UserIdto dto)
    {
        var entity = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
        if(entity == null)
            throw ApiException.NotFound("User not found");
        entity.FirstName = dto.FirstName;
        entity.LastName = dto.LastName;
        entity.Email = dto.Email;
        entity.Password = DoHash(dto.Password);
        entity.MobileCountryCode = dto.MobileCountryCode;
        entity.MobilePhoneNumber = dto.MobilePhoneNumber;
        entity.IsActive = dto.IsActive;
        await _context.SaveChangesAsync();
    }

    
    private string DoHash(string value)
    {
        using (var alg = SHA512.Create())
        {
            alg.ComputeHash(Encoding.UTF8.GetBytes(value));
            return Convert.ToBase64String(alg.Hash);    
        };
    }
}