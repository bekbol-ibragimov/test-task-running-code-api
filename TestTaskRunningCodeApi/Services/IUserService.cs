﻿using TestTaskRunningCodeApi.Dtos;

namespace TestTaskRunningCodeApi.Services;

public interface IUserService
{
    Task<IEnumerable<UserOdto>> GetList();
    Task<UserOdto> Add(UserIdto dto);
    Task Edit(int id, UserIdto dto);
}