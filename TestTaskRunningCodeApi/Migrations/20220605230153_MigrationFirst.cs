﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TestTaskRunningCodeApi.Migrations
{
    public partial class MigrationFirst : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: false),
                    MobileCountryCode = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: false),
                    MobilePhoneNumber = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedDate", "Email", "FirstName", "IsActive", "LastName", "MobileCountryCode", "MobilePhoneNumber", "Password", "UpdatedDate" },
                values: new object[] { 1, new DateTime(2022, 6, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "ronaldo@test.net", "Cristiano", true, "Ronaldo", "12", "45645645645", "J1fLPK/DmvRRq7Jpe+ebSrYdY9dNhbBBhinejCaBG1KfPzeA0BUAY/9Vor7udMTsECoqJzGh8ffxDUc60Ypqhw==", new DateTime(2022, 6, 6, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedDate", "Email", "FirstName", "IsActive", "LastName", "MobileCountryCode", "MobilePhoneNumber", "Password", "UpdatedDate" },
                values: new object[] { 2, new DateTime(2022, 6, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "messi@test.net", "Lionel", true, "Messi", "54", "234234333334", "ujJTh2rta8ItSm/1PYQGxq2GQZXtFEq1yHYhtsIztUi66uaVbfNG7IwX9eoQ817jy8UUeX7X3dMUVGTioLq0Ew==", new DateTime(2022, 6, 6, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedDate", "Email", "FirstName", "IsActive", "LastName", "MobileCountryCode", "MobilePhoneNumber", "Password", "UpdatedDate" },
                values: new object[] { 3, new DateTime(2022, 6, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "maradona@test.net", "Diego", false, "Maradona", "43", "23423532523", "ujJTh2rta8ItSm/1PYQGxq2GQZXtFEq1yHYhtsIztUi66uaVbfNG7IwX9eoQ817jy8UUeX7X3dMUVGTioLq0Ew==", new DateTime(2022, 6, 6, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
