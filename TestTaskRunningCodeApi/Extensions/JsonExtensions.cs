﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace TestTaskRunningCodeApi.Extensions
{
    public static class JsonExtensions
    {
        public static string ToJson<T>(this T obj, bool beautify = false)
            where T : class
        {
            return JsonConvert.SerializeObject(obj, beautify ? Formatting.Indented : Formatting.None);
        }

        public static string ToCamelCaseJson<T>(this T obj) where T : class
        {
            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                NullValueHandling = NullValueHandling.Ignore
            };
            return JsonConvert.SerializeObject(obj, serializerSettings);
        }
        
        public static T FromJson<T>(this string str) where T : class
        {
            return JsonConvert.DeserializeObject<T>(str);
        }
    }
}