﻿using System.Net;

namespace TestTaskRunningCodeApi.Exceptions
{
    public class ApiException : Exception
    {
        public int StatusCode { get; set; }
        public string Error { get; set; }
        
        public ApiException(HttpStatusCode statusCode,  string message): base(message)
        {
            StatusCode = (int) statusCode;
            Error = message;
        }

        public static ApiException BadRequest()
        {
            return new ApiException(HttpStatusCode.BadRequest, "Bad Request");
        }
        public static ApiException BadRequest(string message)
        {
            return new ApiException(HttpStatusCode.BadRequest, message);
        }

        public static ApiException Unauthorized()
        {
            return new ApiException(HttpStatusCode.Unauthorized, "Unauthorized");
        }
        public static ApiException NotFound()
        {
            return new ApiException(HttpStatusCode.NotFound, "Not Found");
        }
        public static ApiException NotFound(string message)
        {
            return new ApiException(HttpStatusCode.NotFound, message);
        }
        public static ApiException NotFoundParam(string param)
        {
            return new ApiException(HttpStatusCode.NotFound, $"Not Found {param}");
        }
        
        public static ApiException NotFound(string param, string values)
        {
            return new ApiException(HttpStatusCode.NotFound, $"Not Found {param}: {values}");
        }
        
        public static ApiException MethodNotAllowed(string message)
        {
            return new ApiException(HttpStatusCode.MethodNotAllowed, message);
        }
        
        public static ApiException Conflict (string message)
        {
            return new ApiException(HttpStatusCode.Conflict, message);
        }
        
        public static ApiException TooManyRequests (string message)
        {
            return new ApiException(HttpStatusCode.TooManyRequests, message);
        }
        
        public static ApiException UnprocessableEntity (string message)
        {
            return new ApiException(HttpStatusCode.UnprocessableEntity, message);
        }
    }
    
    
}