﻿using TestTaskRunningCodeApi.Dtos;
using TestTaskRunningCodeApi.Exceptions;
using TestTaskRunningCodeApi.Extensions;

namespace TestTaskRunningCodeApi.Middleware;

public class ExceptionMiddleware
{
    private readonly RequestDelegate _next;

    public ExceptionMiddleware(RequestDelegate next)
    {
        _next = next;
    }
    
    public async Task InvokeAsync(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (ApiException ex)
        {
            if (httpContext?.Response != null)
            {
                httpContext.Response.ContentType = "application/problem+json; charset=utf-8";
                httpContext.Response.StatusCode = ex.StatusCode;
            }
            var result = new ResponseExceptionDto
            {
                StatusCode = ex.StatusCode,
                Message = ex.Error
            }.ToCamelCaseJson();
            await httpContext.Response.WriteAsync(result);
        }
        catch (Exception ex)
        {
            var result = new ResponseExceptionDto
            {
                StatusCode = 500,
                Message = ex.Message
            }.ToCamelCaseJson();
            await httpContext.Response.WriteAsync(result);
        }
    }

}