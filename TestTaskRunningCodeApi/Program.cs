using Microsoft.EntityFrameworkCore;
using TestTaskRunningCodeApi.Entities;
using TestTaskRunningCodeApi.Middleware;
using TestTaskRunningCodeApi.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var connection =  builder.Configuration.GetConnectionString("MsSqlConnection");
Console.WriteLine("MsSqlConnection: {0}", connection);
builder.Services.AddDbContext<IAppDbContext, AppDbContext>(options =>
    options.UseSqlServer( 
        connection,
        b => b.MigrationsAssembly(typeof(AppDbContext).Assembly.FullName)));

builder.Services.AddTransient<IUserService, UserService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.UseMiddleware<ExceptionMiddleware>();

using (var scope = app.Services.CreateScope())
{
    Console.WriteLine("Add migration");
    var dataContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();
    dataContext.Database.Migrate();
}

app.Run();
