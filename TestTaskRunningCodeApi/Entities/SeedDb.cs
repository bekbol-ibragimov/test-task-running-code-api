﻿using Microsoft.EntityFrameworkCore;

namespace TestTaskRunningCodeApi.Entities;

public class SeedDb
{
    public static void SetUsers(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>().HasData(
            new User()
            {
                Id = 1,
                FirstName = "Cristiano",
                LastName = "Ronaldo",
                Email = "ronaldo@test.net",
                Password = "J1fLPK/DmvRRq7Jpe+ebSrYdY9dNhbBBhinejCaBG1KfPzeA0BUAY/9Vor7udMTsECoqJzGh8ffxDUc60Ypqhw==",
                IsActive = true,
                MobileCountryCode = "12",
                MobilePhoneNumber = "45645645645",
                CreatedDate = new DateTime(2022, 6, 6),
                UpdatedDate = new DateTime(2022, 6, 6),
            },
            new User()
            {
                Id = 2,
                FirstName = "Lionel",
                LastName = "Messi",
                Email = "messi@test.net",
                Password = "ujJTh2rta8ItSm/1PYQGxq2GQZXtFEq1yHYhtsIztUi66uaVbfNG7IwX9eoQ817jy8UUeX7X3dMUVGTioLq0Ew==",
                IsActive = true,
                MobileCountryCode = "54",
                MobilePhoneNumber = "234234333334",
                CreatedDate = new DateTime(2022, 6, 6),
                UpdatedDate = new DateTime(2022, 6, 6),
            },
            new User()
            {
                Id = 3,
                FirstName = "Diego",
                LastName = "Maradona",
                Email = "maradona@test.net",
                Password = "ujJTh2rta8ItSm/1PYQGxq2GQZXtFEq1yHYhtsIztUi66uaVbfNG7IwX9eoQ817jy8UUeX7X3dMUVGTioLq0Ew==",
                IsActive = false,
                MobileCountryCode = "43",
                MobilePhoneNumber = "23423532523",
                CreatedDate = new DateTime(2022, 6, 6),
                UpdatedDate = new DateTime(2022, 6, 6),
            }
        );
    }
}