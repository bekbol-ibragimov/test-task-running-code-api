﻿using Microsoft.EntityFrameworkCore;

namespace TestTaskRunningCodeApi.Entities;

public class AppDbContext: DbContext, IAppDbContext
{
    public AppDbContext()
    {
    }
        
    public AppDbContext(DbContextOptions<AppDbContext> options)
        : base(options)
    {
    }
    
    public DbSet<User> Users { get; set; }
    
    
    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        var dt = DateTime.UtcNow;
        foreach (var entry in ChangeTracker.Entries<BaseEntity>())
        {
            switch (entry.State)
            {
                case EntityState.Added:
                    entry.Entity.CreatedDate = dt;
                    entry.Entity.UpdatedDate = dt;
                    break;
                case EntityState.Modified:
                    entry.Entity.UpdatedDate = dt;
                    break;
            }
        }
        return base.SaveChangesAsync(cancellationToken);
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
       
        base.OnModelCreating(modelBuilder);
        SeedDb.SetUsers(modelBuilder);
    }
}