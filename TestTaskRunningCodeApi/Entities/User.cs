﻿using System.ComponentModel.DataAnnotations;

namespace TestTaskRunningCodeApi.Entities;

public class User: BaseEntity
{
    [Required]
    [MaxLength(200)]
    public string FirstName { get; set; }
    
    [Required]
    [MaxLength(100)]
    public string LastName { get; set; }
    
    [Required]
    [MaxLength(150)]
    public string Email { get; set; }
    
    [Required]
    [MaxLength(400)]
    public string Password { get; set; }
    
    [MaxLength(3)]
    public string MobileCountryCode { get; set; }
    
    [MaxLength(20)]
    public string MobilePhoneNumber { get; set; }
    
    public bool IsActive { get; set; }
}