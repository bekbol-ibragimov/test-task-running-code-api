﻿using Microsoft.EntityFrameworkCore;

namespace TestTaskRunningCodeApi.Entities;

public interface IAppDbContext
{
    DbSet<User> Users { get; set; }
}