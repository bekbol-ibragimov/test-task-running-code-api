﻿using System.ComponentModel.DataAnnotations;

namespace TestTaskRunningCodeApi.Dtos;

public class UserIdto
{
    [Required]
    [MaxLength(200)]
    public string FirstName { get; set; }
    
    [Required]
    [MaxLength(100)]
    public string LastName { get; set; }
    
    [Required]
    [EmailAddress]
    [MaxLength(150)]
    public string Email { get; set; }
    
    [Required]
    [MaxLength(50)]
    public string Password { get; set; }
    
    [MaxLength(3)]
    [RegularExpression("^[0-9]+$")]
    public string MobileCountryCode { get; set; }
    
    [MaxLength(20)]
    [RegularExpression("^[0-9]+$")]
    public string MobilePhoneNumber { get; set; }
    [Required]
    public bool IsActive { get; set; }
}