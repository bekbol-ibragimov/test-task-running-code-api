﻿namespace TestTaskRunningCodeApi.Dtos;

public class ResponseExceptionDto
{
    public int StatusCode { get; set; }
    public string Message { get; set; }
}