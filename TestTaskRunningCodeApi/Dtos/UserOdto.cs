﻿using System.ComponentModel.DataAnnotations;

namespace TestTaskRunningCodeApi.Dtos;

public class UserOdto
{
    public int Id { get; set; }
    public DateTime CreatedDate { get; set; }
    public DateTime UpdatedDate { get; set; }
    [Required]
    [MaxLength(200)]
    public string FirstName { get; set; }
    
    [Required]
    [MaxLength(100)]
    public string LastName { get; set; }
    
    [Required]
    [MaxLength(150)]
    public string Email { get; set; }
    
    [MaxLength(3)]
    public string MobileCountryCode { get; set; }
    
    [MaxLength(20)]
    public string MobilePhoneNumber { get; set; }
    
    public bool IsActive { get; set; }
}